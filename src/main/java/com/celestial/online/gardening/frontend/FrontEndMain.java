/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.celestial.online.gardening.frontend;

import com.celestial.online.gardening.common.BusinessObjectRenderer;
import com.celestial.online.gardening.common.KeyboardHandler;
import com.celestial.online.gardening.common.ShutdownHandler;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import com.s2s.mom.services.ObjectReconstructionFactory;
import com.s2s.mom.services.Publisher;
import com.s2s.mom.services.RabbitMQConfig;
import com.s2s.testobjects.Student;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.celestial.online.gardening.bos.Account;
import com.celestial.online.gardening.bos.Customer;
import com.celestial.online.gardening.bos.Item;
import com.celestial.online.gardening.common.InputChannel;
import com.celestial.online.gardening.bos.Order;
import com.celestial.online.gardening.bos.Product;
import com.celestial.online.gardening.bos.CustomerAccountMgrFactory;
import com.celestial.online.gardening.bos.ICustomerAccountManager;
import com.celestial.online.gardening.common.SerializableFileWriter;
import com.celestial.online.gardening.common.Warehouse;
import com.celestial.online.gardening.common.Writer;
import com.celestial.online.gardening.common.Reader;

/**
 *
 * @author Selvyn
 */
public class FrontEndMain
{

    private static final ObjectReconstructionFactory orf = new ObjectReconstructionFactory();
    private static final String EXCHANGE_NAME = "students";
    private static final String HOST_ID = "192.168.99.100";
    private static final String ERROR_QUEUE_NAME = "client_errors";
    private static final String RESPONSE_QUEUE = "server_response";

    private RabbitMQConfig pubsubMQ;
    private Publisher pubsub_publisher;
    private RabbitMQConfig errorMQ;
    private Publisher error_publisher;
    private RabbitMQConfig serverResponseMQ;
    private Publisher serverResponse_publisher;

    private final CustomerAccountMgrFactory camFactory = new CustomerAccountMgrFactory(); 

    
    public static void main(String[] args) throws Exception
    {
        FrontEndMain munit = new FrontEndMain();
        ShutdownHandler sdh = new ShutdownHandler();

        KeyboardHandler kh = KeyboardHandler.getInstance();

        kh.addCommand("save", () ->
        {
            System.out.println("Save command called");
            munit.test_serialization_writer_all_objects();
        });

        kh.addCommand("load", () ->
        {
            System.out.println("Load command called");
            munit.test_serialization_reader_all_objects();
        });

        kh.addCommand("create", () ->
        {
            System.out.println("Create command called");
            munit.bootstrap_products_items();
            munit.bootstrap_customers_accounts();
            munit.bootstrap_orders_accounts_items();
        });

        kh.addCommand("new customer", () ->
        {
            System.out.println("New Customer command called");
            munit.create_new_customer_from_data_entry();
        });

        kh.addCommand("show customers", () ->
        {
            System.out.println("Show Customers command called");
            munit.show_all_customers();
        });

        kh.addCommand("key", () ->
        {
            System.out.println("Key command called");
            String key = (new CustomerAccountMgrFactory()).getCustomerAccountMgr().genkey();
            System.out.println(key);
        });

        kh.addCommand("show commands", () ->
        {
            System.out.println("Show Command command called");
            munit.show_commands();
        });

        kh.addCommand("send error", () ->
        {
            System.out.println("Send Error command called");
            munit.send_error_message();
        });

        kh.addCommand("send response", () ->
        {
            System.out.println("Send Response command called");
            munit.send_response_message();
        });
        
        kh.addCommand("send broadcast", () ->
        {
            System.out.println("Send Broadcast command called");
            munit.send_broadcast_message();
        });
         
        munit.bootStrapPubSubMQ();
        munit.bootStrapErrorMQ();
        munit.bootStrapClientResponseMQ();
        munit.setupEventHandlers();
        munit.setupMQEventHandlers();

        // This should be the last command we call as it is a blocking call
        sdh.waitForShutdownCommand();

        munit.pubsubMQ.close();
        munit.errorMQ.close();
        munit.serverResponseMQ.close();
    }

    private void show_commands()
    {
        Set<String> commands = KeyboardHandler.getInstance().getCommands();
        System.out.println("Available commands:");

        commands.forEach((entry) ->
        {
            System.out.println("\t" + entry);
        });
    }

    private LocalDate stringToLocalDate(String theDate)
    {
        LocalDate ldate = null;
        try
        {
            SimpleDateFormat dtf = new SimpleDateFormat("dd/MM/yyyy");
            Date dd = dtf.parse(theDate);

            ldate = dd.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        } catch (ParseException ex)
        {
            Logger.getLogger(FrontEndMain.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ldate;
    }

    public void fiddlewithDate()
    {

        System.out.print("DOB (DD/MM/YYYY): ");
        String dob = KeyboardHandler.getInstance().readString();
        LocalDate ld = stringToLocalDate(dob);

        System.out.println(ld);
    }

    //==========================================================================
    // RabbitMQ Methods
    //==========================================================================
    public void bootStrapPubSubMQ() throws IOException, TimeoutException
    {
        pubsubMQ = new RabbitMQConfig(HOST_ID, EXCHANGE_NAME); //pub/sub channel
        pubsub_publisher = pubsubMQ.initMQSubscriber();

        // Once a channel has been created, you can send messages to yourself on it...
        // This just tests the channel is functioning correctly
        String message = "Sending a test message to pubsub channel";
        pubsub_publisher.sendToExchange(message);
    }

    public void bootStrapErrorMQ() throws IOException, TimeoutException
    {
        errorMQ = new RabbitMQConfig(HOST_ID, ERROR_QUEUE_NAME); //P2P channel
        error_publisher = errorMQ.initMQSimpleChannel();

        // Once a channel has been created, you can send messages to yourself on it...
        // This just tests the channel is functioning correctly
        String message = "Sending a test message to error channel";
        error_publisher.sendToQueue(message);
    }

    public void bootStrapClientResponseMQ() throws IOException, TimeoutException
    {
        serverResponseMQ = new RabbitMQConfig(HOST_ID, RESPONSE_QUEUE); //P2P channel
        serverResponse_publisher = serverResponseMQ.initMQSimpleChannel();

        // Once a channel has been created, you can send messages to yourself on it...
        // This just tests the channel is functioning correctly
        String message = "Sending a test message to client channel";
        serverResponse_publisher.sendToQueue(message);
    }

    public void setupEventHandlers()
    {
        // Create your handlers for the objects you expect to receive
        orf.registerHandler(Student.class.getTypeName(), (Object body) ->
        {
            Student std = (Student) body;
            System.out.println("Student: " + std.getFname() + "/" + std.getLname());
        });
        orf.registerHandler(String.class.getTypeName(), (Object body) ->
        {
            String msg = (String) body;
            System.out.println("Message: " + msg);
        });
        //++ todo
        // Register handlers for the Business Domain objects you want to 
        // receive from the server
    }

    public void setupMQEventHandlers() throws IOException
    {
        // Setup pubsub callback
        // This is the channel onto which the server will push new customer objects
        Channel channel = pubsubMQ.getChannel();
        String queueName = pubsubMQ.getQueueName();

        System.out.println(" [*] Waiting for messages on: " + queueName + ". To exit press CTRL+C");
       
        //++ 
        // todo create the DefaultConsumer for the channel above
        // see below for examples of a DefaultConsumer being created
        DefaultConsumer consumer = null;
        
        // The next call spawns a seperate thread so no need to create a thread here
        // to manage this call, it returns immediately
        channel.basicConsume(queueName, true, consumer);

        // Setup error queue callback handler
        channel = error_publisher.getChannel();
        queueName = errorMQ.getQueueName();

        System.out.println(" [*] Waiting for messages on: " + queueName + ". To exit press CTRL+C");

        consumer = new DefaultConsumer(channel)
        {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope,
                    AMQP.BasicProperties properties, byte[] body) throws IOException
            {
                orf.pushEvent(body);
            }
        };
        channel.basicConsume(queueName, true, consumer);

        // Setup client response callback handler
        channel = serverResponse_publisher.getChannel();
        queueName = serverResponseMQ.getQueueName();

        System.out.println(" [*] Waiting for messages on: " + queueName + ". To exit press CTRL+C");

        consumer = new DefaultConsumer(channel)
        {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope,
                    AMQP.BasicProperties properties, byte[] body) throws IOException
            {
                orf.pushEvent(body);
            }
        };
        channel.basicConsume(queueName, true, consumer);
    }

    public void send_error_message()
    {
        KeyboardHandler kbh = KeyboardHandler.getInstance();
        System.out.print("Enter an Error Message: ");
        String message = kbh.readString();
        try
        {
            error_publisher.sendToQueue(message);
        } catch (IOException ex)
        {
            Logger.getLogger(FrontEndMain.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void send_response_message()
    {
        KeyboardHandler kbh = KeyboardHandler.getInstance();
        System.out.print("Enter a Response Message: ");
        String message = kbh.readString();
        try
        {
            serverResponse_publisher.sendToQueue(message);
        } catch (IOException ex)
        {
            Logger.getLogger(FrontEndMain.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void send_broadcast_message()
    {
        KeyboardHandler kbh = KeyboardHandler.getInstance();
        System.out.print("Enter a Broadcast Message: ");
        String message = kbh.readString();
        try
        {
            pubsub_publisher.sendToExchange(message);
        } catch (IOException ex)
        {
            Logger.getLogger(FrontEndMain.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //==========================================================================
    // Business Object Methods
    //==========================================================================
    public void bootstrap_products_items()
    {
        // This call will bootstrap the products
        Warehouse.getInstance();
    }

    public void bootstrap_customers_accounts()
    {
        // Create the accounts first and Customers
        Account acc = null;
        Customer cc = null;

        cc = new Customer("Sevlyn", "Wright", LocalDate.of(1965, 5, 8), "Somewhere in Birmingham");
        camFactory.getCustomerAccountMgr().addCustomer("C1", cc);
        acc = new Account(cc, 1);
        cc.setAccount(acc); // tie acc to the customer
        camFactory.getCustomerAccountMgr().addAccount(acc);

        cc = new Customer("Samuel", "Wright", LocalDate.of(1982, 4, 22), "Somewhere else in Birmingham");
        camFactory.getCustomerAccountMgr().addCustomer("C2", cc);
        acc = new Account(cc, 2);
        cc.setAccount(acc); // tie acc to the customer
        camFactory.getCustomerAccountMgr().addAccount(acc);

        cc = new Customer("Graham", "Meaden", LocalDate.of(1967, 4, 3), "Somewhere down South");
        camFactory.getCustomerAccountMgr().addCustomer("C3", cc);
        acc = new Account(cc, 3);
        cc.setAccount(acc); // tie acc to the customer
        camFactory.getCustomerAccountMgr().addAccount(acc);
    }

    public void create_new_customer_from_data_entry()
    {
        KeyboardHandler kbh = KeyboardHandler.getInstance();
        System.out.print("First Name: ");
        String fname = kbh.readString();
        System.out.print("Last Name: ");
        String lname = kbh.readString();
        System.out.print("Addres: ");
        String address = kbh.readString();
        System.out.print("DOB (DD/MM/YY): ");
        String dob = kbh.readString();
        LocalDate ldob = stringToLocalDate(dob);
        String customerKey = camFactory.getCustomerAccountMgr().genkey();

        int accNo = camFactory.getCustomerAccountMgr().nextAccountNo();

        // Create the accounts first and Customers
        Account acc = null;
        Customer cc = null;

        cc = new Customer(fname, lname, ldob, address);
        camFactory.getCustomerAccountMgr().addCustomer(customerKey, cc);
        acc = new Account(cc, accNo);
        cc.setAccount(acc); // tie acc to the customer
        camFactory.getCustomerAccountMgr().addAccount(acc);
    }

    /*
     * This method will use an Order object to link the Item and Product objects 
     * to the Customer and Account objects
     */
    public void bootstrap_orders_accounts_items()
    {
        // Get an account and a product
        Account acc = camFactory.getCustomerAccountMgr().getAccount(1);
        Product pp = Warehouse.getInstance().getProduct("JDF-001");

        // only proceed if the keys were valid
        if (acc != null && pp != null)
        {
            try
            {
                // create new order giving it the order number of 1
                Order oo = new Order(acc, 1, LocalDate.now());

                // add the order the account
                acc.addOrder(oo);

                // Use the product to purchase X times the Product
                Item itm = pp.purchaseItem(3);

                // Only proceed if you used a valid item id
                if (itm != null)
                {
                    oo.addItem(itm);  // this call decrease the stock for this product
                }
            } catch (Exception ex)
            {
                Logger.getLogger(FrontEndMain.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void test_serialization_writer_all_objects()
    {
        String fname = "c:\\tmp\\all-objects.oos";

        // working with a Serializable File Writer
        SerializableFileWriter sfw = Writer.getSerializableFileWriter(fname);

        // Write the object out
        // HashMap are Serializable, so it can be written straight out
        // Because all objects in the HashMap are Serializable, they will all be
        // written out as well
        sfw.write(camFactory.getCustomerAccountMgr());

        sfw.close();
    }

    public void test_serialization_reader_all_objects()
    {
        String fname = "c:\\tmp\\all-objects.oos";
        // Read the object from the filesystem
        InputChannel sfr = Reader.getSerializableFileReader(fname);
        sfr.openForReading(fname);

        // Read the entire object network back in
        ICustomerAccountManager caMgr = (ICustomerAccountManager)sfr.read();
        //++ todo 
        HashMap< String, Customer> customersIn = camFactory.getCustomerAccountMgr().getCustomers();

        // Only process the objects if the read was successful
        if (customersIn != null)
        {
            // Iterate over all the Customers...
            for (Map.Entry<String, Customer> entry : customersIn.entrySet())
            {
                Customer cc = entry.getValue();

                System.out.printf("Customer code ==> %s\n", entry.getKey());
                cc.getRenderer().display();
                cc.getAccount().getRenderer().display();

                displayOrderDetais(cc.getAccount());
            }
        }
    }

    public void displayOrderDetais(Account acc)
    {
        HashMap< Integer, Order> orders = acc.getOrders();
        for (Map.Entry<Integer, Order> entry : orders.entrySet())
        {
            Order ord = entry.getValue();
            BusinessObjectRenderer handler = new BusinessObjectRenderer()
            {
                @Override
                public void display()
                {
                    System.out.printf("Order\tORDER NO\tTOTAL COSR\tORDER DATE\n");
                    System.out.printf("\t%d\t\t%.2f\t\t%s\n",
                            ord.getOrderNo(),
                            ord.getTotalCost(),
                            ord.getDateCreated().toString());
                }

            };
            handler.display();

            displayItemDetails(ord);
        }
    }

    public void displayItemDetails(Order ord)
    {
        for (Item itm : ord.getItems())
        {
            BusinessObjectRenderer handler = new BusinessObjectRenderer()
            {
                @Override
                public void display()
                {
                    System.out.printf("Item\tUNIQUE ID\tQUANTITY\tCOST\n");
                    System.out.printf("\t%d\t\t%d\t\t%.2f\n",
                            itm.getUniqueId(),
                            itm.getQuantity(),
                            itm.getItemCost());
                }
            };
            handler.display();

            displayProductDetails(itm.getProduct());
        }
    }

    public void displayProductDetails(Product pp)
    {
        BusinessObjectRenderer handler = () ->
        {
            System.out.println("Product\tPRODUCT CODE\tDESCRIPTION\t\tPRICE\tQUANTITY");
            System.out.printf("\t%s\t\t%s\t\t%.2f\t%d\n",
                    pp.getProductCode(),
                    pp.getDescription(),
                    pp.getPrice(),
                    pp.getQuantity());
        };
        handler.display();
    }

    public void show_all_customers()
    {
        HashMap<String, Customer> customers = camFactory.getCustomerAccountMgr().getCustomers();
        for (Map.Entry<String, Customer> entry : customers.entrySet())
        {
            Customer cc = entry.getValue();

            System.out.printf("Customer code ==> %s\n", entry.getKey());
            cc.getRenderer().display();
            cc.getAccount().getRenderer().display();

            displayOrderDetais(cc.getAccount());
        }
    }
}
